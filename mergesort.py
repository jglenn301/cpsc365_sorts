def mergesort(a):
    if len(a) == 1:
        return a

    left = mergesort(a[:len(a) // 2])
    right = mergesort(a[len(a) // 2:])
    return merge(left, right)


def merge(left, right):
    i = 0
    j = 0
    result = []
    while i < len(left) or j < len(right):
        if j == len(right) or (i < len(left) and left[i] <= right[j]):
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
    return result

if __name__ == "__main_":
    print(mergesort([1, 2, 3, 4, 5]))
    print(mergesort([5, 1, 2, 4, 3, 7, 8, 6, 9, 0]))
    print(mergesort([5, 4, 3, 2, 1]))
