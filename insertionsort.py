def insertionsort(a):
    i = 1
    while i < len(a):
        insert(a, i)
        i = i + 1
    return a


def insert(a, i):
    k = i
    while k > 0 and a[k-1] > a[k]:
        temp = a[k-1]
        a[k-1] = a[k]
        a[k] = temp
        k = k - 1


if __name__ == "__main_":
    print(insertionsort([1, 2, 3, 4, 5]))
    print(insertionsort([5, 1, 2, 4, 3, 7, 8, 6, 9, 0]))
    print(insertionsort([5, 4, 3, 2, 1]))
