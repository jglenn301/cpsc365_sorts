def binary_search(a, key):
    start = 0
    end = len(a) - 1
    while start <= end and a[(start + end) // 2] != key:
        mid = (start + end) // 2
        if key < a[mid]:
            end = mid - 1
        else:
            start = mid + 1
    if start > end or a[(start + end) // 2] != key:
        return False
    else:
        return (start + end) // 2


def test_search(a):
    print(a)
    for key in range(a[0] - 1, a[len(a) - 1] + 2):
        print(key, binary_search(a, key))


if __name__ == "__main__":
    test_search([1, 3, 4, 6, 8, 11, 12, 13, 15, 16])
            
